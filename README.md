# Random Gift

Ce projet a été réalisé dans le cadre d'un project d'école (Supinfo) en 2018-2019.

Le but est de créer une base de donnée utilisateur, gérer les opérations CRUD (Create, Read, Update, Delete) des utilisateurs, et pouvoir choisir un utilisateur gagnant au hasard pour ensuite selectionner un produit dans la liste de souhait au hasard aussi. 

Une explication plus detaillée du projet se trouve dans `sujet.pdf`.


## Pré-requis
* [Visual Studio](https://visualstudio.microsoft.com/fr/)
* Installer l'outil `Développement multiplatforme .NET Core` dans Visual Studio Installer
* Installer l'outil `Stockage et traitement des données` dans Visual Studio Installer

## Lancer le projet
1. Ouvrez le projet `Random` avec Visual Studio
2. Ouvez le fichier `App.config`
3. Trouvez cette attribute `attachdbfilename=C:\Users\user\Desktop\random-gift-supinfo/RandomGift\localDatabase.mdf`
4. Remplacez la valeur par votre chemin d'accès à vous au fichier localDatabase.mdf qui se trouve dans le dossier `RandomGift`
5. Une fois fait, vous pouvez lancez le projet

## Démo 
Vous pouvez trouvez une démo du projet via ce [lien](https://drive.google.com/file/d/15zoqKRBQWUGHMwbDMcd7LrBik1U2APsZ/view?usp=sharing)